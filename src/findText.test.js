const findText = require('./findText');
const getFileContent = require('./getFileContent');

jest.mock('./getFileContent');

describe('findText Function', () => {
  it('should resolve to true if the text is found in the file content', async () => {
    getFileContent.mockResolvedValue('This is some sample text to find in the file.');

    const filePath = './test.txt';
    const textToFind = 'sample text';
    const result = await findText(filePath, textToFind);

    expect(result).toBe(true);
  });

  it('should resolve to false if the text is not found in the file content', async () => {
    getFileContent.mockResolvedValue('This file does not contain the text to find.');
    const filePath = './test.txt';
    const textToFind = 'bananas';
    const result = await findText(filePath, textToFind);

    expect(result).toBe(false);
  });

  it('should reject the promise if getFileContent rejects', async () => {

    getFileContent.mockRejectedValue(new Error('File not found'));
    const filePath = './teeest.txt';
    const textToFind = 'sample text';

    await expect(findText(filePath, textToFind)).rejects.toThrow('File not found');
  });
});



