const startServer = require("./simpleTestApp");
const request = require("supertest");

let server;

beforeAll(async () => {
  server = await startServer();
});

afterAll(async () => {
  if (server) {
    server.stop();
  }
});

describe('HTTP Server Endpoints', () => {
  it('GET / should return an empty array initially', async () => {
    const response = await request(server).get('/');
    expect(response.status).toBe(200);
    expect(response.body).toEqual([]);
  });

  it('POST / should add a task to the list', async () => {
    const task = {
      title: 'Test Task',
      description: 'This is a test task.',
    };

    const response = await request(server).post('/').send(task);
    expect(response.status).toBe(200);

    const getResponse = await request(server).get('/');
    expect(getResponse.body).toHaveLength(1);
    expect(getResponse.body[0].title).toBe(task.title);
    expect(getResponse.body[0].description).toBe(task.description);
  });

  it('POST / should return 400 Bad Request for invalid input', async () => {
    const invalidTask = {
      title: 'Te', 
      description: '', 
    };

    const response = await request(server).post('/').send(invalidTask);
    expect(response.status).toBe(400);
  });
});