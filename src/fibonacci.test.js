const getFibonacci = require("./fibonacci");

describe('Fibonacci Function', () => {
    it('should return 0 for n <= 0', () => {
      expect(getFibonacci(0)).toBe(0);
      expect(getFibonacci(-5)).toBe(0);
    });
  
    it('should return 1 for n = 1', () => {
      expect(getFibonacci(1)).toBe(1);
    });
  
    it('should return 1 for n = 2', () => {
      expect(getFibonacci(2)).toBe(1);
    });
  
    it('should return the correct Fibonacci number for n = 5', () => {
      expect(getFibonacci(5)).toBe(5);
    });
  
    it('should return the correct Fibonacci number for n = 10', () => {
      expect(getFibonacci(10)).toBe(55);
    });
  });
  
  
  
  
  
  
  